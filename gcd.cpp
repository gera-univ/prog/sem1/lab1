#include <iostream>
#include <sstream>

#define MAX_N 6

using namespace std;

int euclgcd(int a, int b)  {
    // Computes the GCD of two integers a, b > 0
    while (a != b)  {
        if (a>b)
            a -= b;
        else
            b -= a;
    }
    return a;
}

long lcd(int a, int b) {
    return (long)a*b/euclgcd(a,b);
}

istringstream safe_int_input() {
    string s;
    cin >> s;
    if (s[0] == '-') {
        cout << "The value must be positive";
    }
    int n;
    for (n = 0; n < s.size(); ++n)    {
        if (!isdigit(s[n])) {
            cout << "Incorrect input! A positive integer value was expected!\n";
            return safe_int_input();
        }
    }
    if (n > MAX_N) {
        cout << "The number entered is too big!\n";
        return safe_int_input();
    }
    istringstream ss(s);
    return ss;
}

int main()  {
    int a, b;
    cout << "Enter two integers less than 10^7:\n";
    safe_int_input() >> a;
    cout << "-------\n";
    safe_int_input() >> b;
    cout << "GCD("<<a<<", "<<b<<") = " << euclgcd(a,b) <<
     "\nLCD("<<a<<", "<<b<<") = " << lcd(a,b) << endl;
    return 0;
}
