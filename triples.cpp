#include <iostream>
#include <cmath>

using namespace std;

long euclgcd(long a, long b)  {
    // Computes the GCD of two integers a, b > 0
    while (a != b)  {
        if (a>b)
            a -= b;
        else
            b -= a;
    }
    return a;
}

int main()  {
    ios_base::sync_with_stdio(false);
    int N = 10000000;
    int count = 0;

    long m_max = (long)(sqrt(N));
    for (long m = 1; m < m_max; ++m) {
        for (long n = 1; n < m; ++n) {
            if (((m - n) & 1) && euclgcd(m, n) == 1) {
                long m2 = m*m;
                long n2 = n*n;
                long a = m2 - n2;
                if (a > N) continue;
                long b = 2*m*n;
                if (b > N) continue;
                long c = m2 + n2;
                if (c > N) continue;
                for (int k = 1; a*k <= N && b*k <= N && c*k <= N; ++k) {
                    //cout << a*k << ' ' << b*k << ' ' << c*k << '\n';
                    ++count;
                }

            }
        }
    }
    //cout << endl;
    cout << count << endl;
    return 0;
}
