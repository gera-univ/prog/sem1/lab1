#include <iostream>
#include <sstream>

using namespace std;

istringstream safe_int_input(int ndigits) {
    string s;
    cin >> s;
    int n = 0;
    for (int i = 0; i < s.size(); ++i)    {
        if (i == 0 && s[i] == '-') continue;
        if (!isdigit(s[i])) {
            cout << "Incorrect input! An integer value was expected!\n";
            return safe_int_input(ndigits);
        }
        ++n;
    }
    if (n != ndigits) {
        cout << "Incorrect input! A " << ndigits << "-digit integer was expected!\n";
        return safe_int_input(ndigits);
    }
    istringstream ss(s);
    return ss;
}


int main()  {
    cout << "Please enter a 3-digit integer\n";
    int a;
    safe_int_input(3) >> a;
    cout << "The sum of all digits of " << a <<" is " <<  abs(a % 10 + (a/10) % 10 + (a/100) % 10);
    return 0;
}
