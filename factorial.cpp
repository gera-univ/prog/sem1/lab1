#include <iostream>
#include <sstream>

#define MAX_ARGUMENT 20

using namespace std;

int safe_int_input() {
    string s;
    cin >> s;
    for (auto c : s)    {
        if (!isdigit(c)) {
            cout << "Incorrect input! A positive integer value was expected!\n";
            return safe_int_input();
        }
    }
    istringstream ss(s);
    int n;
    ss >> n;
    if (n > MAX_ARGUMENT) {
        cout << "The number entered is too big!\n";
        return safe_int_input();
    }
    return n;
}

long long factorial(int a)   {
    long long f = 1;
    while (a > 1) {f *= a--;};
    return f;
}

int main()  {
    int a;
    cout << "Please enter the integer value of 0 <= a <= 20:\n";
    a = safe_int_input();
    cout << "a! = " << factorial(a);
    return 0;
}
